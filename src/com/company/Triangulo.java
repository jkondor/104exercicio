package com.company;

import javax.swing.*;
import java.util.List;

public class Triangulo extends FormaGeometrica {
    private double ladoA;
    private double ladoB;
    private double ladoC;

    public Triangulo(List<Double> lados) {
        super(lados);
        this.ladoA = lados.get(0);
        this.ladoB = lados.get(1);
        this.ladoC = lados.get(2);
    }

    @Override
    public double calcularArea() {
        Double s = (ladoA + ladoB + ladoC)/2;
        return Math.sqrt(s * (s-ladoA) * (s-ladoB) * (s-ladoC));
    }

    public boolean eTriangulo(){

        if (ladoA < (ladoB + ladoC) && ladoB < (ladoA + ladoC) && ladoC < (ladoA + ladoB)){
            return true;
        } else {
            return false;
        }
    }
}
