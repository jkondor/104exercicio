package com.company;

import java.util.List;

public class Circulo extends FormaGeometrica{
    private double raio;

    public Circulo(List<Double> lados) {
        super(lados);
        raio = lados.get(0);
    }

    @Override
    public double calcularArea() {
        return Math.pow(raio,2)* Math.PI;
    }
}
