package com.company;

import java.util.List;

public class Retangulo extends FormaGeometrica{
    private double altura;
    private double largura;

    public Retangulo(List<Double> lados) {
        super(lados);
        this.altura = lados.get(0);
        this.largura = lados.get(1);
    }
    @Override
    public double calcularArea() {

        return altura * largura;
    }
}
