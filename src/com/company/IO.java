package com.company;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class IO {

    public List<Double> pedirLados(){
        List<Double> lados = new ArrayList<>();

        Scanner leitura = new Scanner(System.in);
        System.out.print("Digite os lados da forma geometrica (digite 0 para terminar):");

        double lado;
        do {
            lado = leitura.nextDouble();
            if (lado!=0d) {
                lados.add(lado);
            }
        } while (lado != 0d);

        return lados;
    }

    public void exibirMensagem(String mensagem){
        System.out.println(mensagem);
    }
}
