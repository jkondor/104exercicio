package com.company;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Controle {
    List<Double> lados = new ArrayList<>();

    public void calcularAreaDaFormaGeometrica(){
        double area = 0;
        String formageometrica = "";
        String mensagem = "";
        IO io = new IO();

        lados = io.pedirLados();

        formageometrica = identificarFormaGeometrica();

        if (formageometrica.equals("")){
            mensagem = "Não é possivel criar forma geometrica e calcular área";
            //System.out.println("Não é possivel criar forma geometrica e calcular área");
        } else  if (formageometrica.equals("Circulo")) {
            Circulo circulo = new Circulo(lados);
            area = circulo.calcularArea();
        } else  if (formageometrica.equals("Retangulo")) {
            Retangulo retangulo = new Retangulo(lados);
            area =  retangulo.calcularArea();
        } else  if (formageometrica.equals("Triangulo")) {
            Triangulo triangulo = new Triangulo(lados);

            boolean eTriangulo = triangulo.eTriangulo();
            if (eTriangulo)  {
                area =  triangulo.calcularArea();
            } else {
                area =  0d;
                formageometrica= "Não é Triangulo";
            }
        }

        if (formageometrica != "") {
            mensagem = "Forma Geometrica: " + formageometrica + " - Área: " + area;
            //System.out.println("Forma Geometrica: " + formageometrica + " - Área: " + area);
        }

        io.exibirMensagem(mensagem);
    }

    public String identificarFormaGeometrica() {
        if(lados.size() == 1){
            return "Circulo";
        } else if (lados.size() == 2) {
            return "Retangulo";
        }else if (lados.size() == 3){
            return "Triangulo";
        } else {
            return "";
        }
    }






}
