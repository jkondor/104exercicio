package com.company;

import java.util.ArrayList;
import java.util.List;

public abstract class FormaGeometrica implements AreaInterface{
    protected List<Double> lados = new ArrayList<>();

    public FormaGeometrica(List<Double> lados) {
        this.lados = lados;
    }
}
